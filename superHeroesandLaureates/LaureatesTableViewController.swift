//
//  LaureatesTableViewController.swift
//  superHeroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class LaureatesTableViewController: UITableViewController {
    
    var laureateHero : [[String:Any]]!
    var laureateSuper:[String:Any]!
    var laureateBoy:[Laureate] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchLaureates()

    }

    // MARK: - Table view data source
    func fetchLaureates(){
        let urlSession = URLSession.shared
        let dropBoxUrl = URL(string: "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1")
        urlSession.dataTask(with: dropBoxUrl!, completionHandler: showLaureates).resume()
    }
    
    func showLaureates(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        print("Inside display laureates method")
        do {
            try laureateHero = JSONSerialization.jsonObject(with:data!, options: .allowFragments) as?
                [[String:Any]]
            for i in 0..<laureateHero.count
            {
                laureateSuper = laureateHero[i]
                let id = laureateSuper["id"] as? String
                let firstName = laureateSuper["firstname"] as? String
                let surName = laureateSuper["surname"] as? String
                let born = laureateSuper["born"] as? String
                let died = laureateSuper["died"] as? String
                laureateBoy.append(Laureate(id: id, firstname: firstName, surname: surName, born: born, died: died))
            }
            
            for laur in laureateBoy{
                print(laur)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                NotificationCenter.default.post(name: NSNotification.Name("Laureats"), object: self.laureateHero)
            }
            //print(self.superhero)
        } catch {
            print(error)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return laureateBoy.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "laureatesTVC", for: indexPath)
        let laureate = laureateBoy[indexPath.row]
        
        let cellTagVariable = cell.viewWithTag(15) as! UILabel
        let cellWithVariable = cell.viewWithTag(25) as! UILabel
        
        cellTagVariable.text = "\(laureate.firstname ?? "NA") \(" ") \(laureate.surname ?? "NA")"
        cellWithVariable.text = "\(laureate.born ?? "NA") \("-")\(laureate.died ?? "NA")"
        return cell
    }


}

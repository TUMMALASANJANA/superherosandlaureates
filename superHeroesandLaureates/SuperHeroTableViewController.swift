//
//  SuperHeroTableViewController.swift
//  superHeroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class SuperHeroTableViewController: UITableViewController {
    
    var hero:[String] = []
    var memberOfHero : [Members] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        retrieveSuperHero()

    }

    // MARK: - Table view data source
    func retrieveSuperHero(){
        let Session = URLSession.shared
        let url = URL(string: "https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json")
        let urlRaw = URL(string: "https://raw.githubusercontent.com/materialsproject/workshop-2017/master/mongo-primer/nobel_laureates.json")
        print(urlRaw!)
        Session.dataTask(with: url!, completionHandler: showSuperHero).resume()
    }
    
    func showSuperHero(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        print("Inside display super hero method")
        do {
            let decoder:JSONDecoder = JSONDecoder()
            let superhero = try decoder.decode(Superhero.self, from: data!)
            self.memberOfHero = superhero.members
            DispatchQueue.main.async {
                self.tableView.reloadData()
                NotificationCenter.default.post(name: NSNotification.Name("superHero"), object: self.memberOfHero)
            }
        } catch {
            print(error)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return memberOfHero.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "superheroTVC", for: indexPath)
        
        // Configure the cell...
        let superHero = memberOfHero[indexPath.row]
        self.hero = superHero.powers
        var superPower : String = ""
        for i in 0..<hero.count
        {
            if i<hero.count-1
            {
                superPower = superPower + "\(hero[i]),"
            }
            else
            {
                superPower = superPower + "\(hero[i])"
            }
        }
        cell.textLabel?.text = "\(superHero.name) (aka: \(superHero.secretIdentity)) "
        cell.detailTextLabel?.text = superPower
        return cell
    }
}


